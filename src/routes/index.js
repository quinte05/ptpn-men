const express = require('express');
const router = express.Router();
const path = require('path');
const {unlink} = require('fs-extra');

const Image = require('../models/Image')

router.get('/', async (req, res) => {
    const images = await Image.find();
    //console.log(images);
    res.render('index', { images });
});

router.get('/upload', async (req, res) => {
    res.render('upload.ejs');
});

router.post('/upload', async (req, res) => {
    const image = new Image();
    image.title = req.body.title;
    image.description = req.body.description;
    image.filename = req.file.filename;
    image.path = '/uploads/' + req.file.filename;
    image.size = req.file.size;
    image.tp = 0;
    await image.save();
    res.redirect('/');
});

router.get('/image/:id/delete', async (req, res) => {
    console.log(req.params.id)
    const image = await Image.findByIdAndDelete(req.params.id)
    await unlink(path.resolve('./src/public' + image.path))
    res.redirect('/');
});

router.get('/edit/:id', async (req, res) => {
  const image = await Image.findById(req.params.id)
  //console.log(req.params.id);
  res.render('edit', { image });
});

router.get('/edit/:id/put', async (req, res) =>{
//console.log(req.params.id)  
  
/*   image.title = req.body.title;
  image.description = req.body.description; */
  //await image.put();
  const image = await Image.findByIdAndUpdate(req.params.id, req.body)
  //console.log(req.params)
  res.redirect('/');
});











router.get('/image/:id', async (req, res) => {
    const image = await Image.findById(req.params.id)
    res.render('profile', { image });
});

router.get('/tp/:id', async (req, res) => {
    //const image = await Image.findById(req.params.id)
    res.redirect('/');
});

/* router.put('/tp/:id', async (req, res) => {
    Image.findByIdAndUpdate(
        { _id: req.params.id },
        { tp: tp++ },
        function(err, result) {
          if (err) {
            res.send(err);
          } else {
            res.send(result);
          }
        }
      );
    //res.redirect('/');
}); */

module.exports = router;