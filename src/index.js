const express = require('express');
const morgan = require('morgan');
const path = require('path');
const multer = require('multer');
const { v4: uuidv4 } = require('uuid');
const { format } = require ('timeago.js');

//Inicializacion
const app = express();
require('./database');

//Configuraciones
app.set('port', process.env.PORT || 6000);
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'ejs')

//Middlewares (Funciones)
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));

const storage = multer.diskStorage({
    destination: path.join(__dirname, 'public/uploads'),
    filename: (req, file, cb) => {
        cb(null, uuidv4() + path.extname(file.originalname).toLocaleLowerCase())
    }
});

app.use(multer({storage: storage}).single('image'));

//Variables Globales
app.use((req,res,next) =>{
    app.locals.format = format;
    next();
})

//Rutas
app.use(require('./routes/index'));

//archivos estaticos
app.use(express.static(path.join(__dirname, 'public')))

//Servidor
app.listen(app.get('port'), () => { 
    console.log('Servidor en puerto', app.get('port'))
});

// cuenta ahorro 70556090726 convenio 09045, $236.000 