const {Schema, model} = require('mongoose');

const imageSchema = new Schema({
    title: { type: String},
    description: {type: String},
    filename: {type: String},
    path: {type: String},
    size: {type: Number},
    created_at: {type: Date, default: Date.now()},
    tp: {type: Number}

});

module.exports = model('Image', imageSchema);